#ifndef GAME_H
#define GAME_H

class Player;
class Board;
class Piece;
class Game{
private:
    Board * shoji_board;
    Player * player_one;
    Player * player_two;
    bool stop_game;
public:
    Game();
    void init_game();
    void play();
    void move_piece(Player *player);
    void reentry_piece(Player *player);
    void promote(Piece *p, int px);
    bool check_point(int px, int py, char dir);
    bool check_final_point(int px,int py);
    void turn(Player *player);
    void find_king(char dir, int *px, int *py);
    bool check_free_boxes(char ,int,int,int,int,char);
    ~Game();
};

#endif // GAME_H
