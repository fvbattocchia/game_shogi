#include "king.h"

King::King(char n, char d):Piece(n,d){

}

bool King::valid_motion(int px_i, int px_f, int py_i, int py_f){
    bool move=false;
    if(((px_f==px_i+1)&&(py_f==py_i))||((px_f==px_i-1)&&(py_f==py_i))||((px_f==px_i)&&(py_f==py_i+1))||((px_f==px_i)&&(py_f==py_i-1))||((px_f==px_i-1)&&(py_f==py_i+1))||((px_f==px_i-1)&&(py_f==py_i-1))||((px_f==px_i+1)&&(py_f==py_i+1))||((px_f==px_i+1)&&(py_f==py_i-1))){
         move=true;
    }
    return move;
}
char King::get_name(){
    return name;
}

char King::get_direcction(){
    return direccion;
}

void King::set_direcction(char dir){
    direccion=dir;
}

bool King::is_promotion()
{
    return false;
}

void King::promote()
{

}

bool King::is_save_promotion()
{
    return false;
}

void King::set_save_promotion(bool s)
{

}

void King::set_original_form()
{

}

bool King::check_promotion(int px){

    return false;
}

void King::set_free_boxes(bool p){


}

bool King::is_check_the_king(int px_i, int px_f, int py_i, int py_f){

    return valid_motion(px_i, px_f, py_i,  py_f);
}
