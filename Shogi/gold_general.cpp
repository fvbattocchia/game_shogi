#include "gold_general.h"

Gold_general::Gold_general(char n, char d):Piece(n,d){

}

bool Gold_general::valid_motion(int px_i, int px_f, int py_i, int py_f){
   bool move=false;
   if(((px_f==px_i)&&(py_f==py_i-1))||((px_f==px_i)&&(py_f==py_i+1))||((px_f==px_i+1)&&(py_f==py_i))||((px_f==px_i-1)&&(py_f==py_i))){
         move=true;
   }else{
        if(direccion=='v'){
            if(((px_f==px_i+1)&&(py_f==py_i-1))||((px_f==px_i+1)&&(py_f==py_i+1))){
                  move=true;
            }
        }else{
            if(((px_f==px_i-1)&&(py_f==py_i-1))||((px_f==px_i-1)&&(py_f==py_i+1))){
                move=true;
            }
        }
   }
   return move;
}
char Gold_general::get_name(){
    return name;
}

char Gold_general::get_direcction(){
    return direccion;
}

void Gold_general::set_direcction(char dir){
    direccion=dir;
}

bool Gold_general::is_promotion()
{
    return false;
}

void Gold_general::promote()
{

}

bool Gold_general::is_save_promotion()
{
    return false;
}

void Gold_general::set_save_promotion(bool s)
{

}

void Gold_general::set_original_form(){


}

bool Gold_general::check_promotion(int px){

    return false;
}

void Gold_general::set_free_boxes(bool p){


}

bool Gold_general::is_check_the_king(int px_i, int px_f, int py_i, int py_f){

    return valid_motion(px_i, px_f, py_i,  py_f);
}
