#include "lance.h"

#define limit_promotion_up 8
#define limit_promotion_down 0
Lance::Lance(char n, char d):Piece(n,d){
    free_boxes=false;
}

bool Lance::valid_motion(int px_i, int px_f, int py_i, int py_f){
    bool move=false;
    if(!promotion){
        if(free_boxes){
            move=true;
            free_boxes=false;
        }
    }else{
        if(((px_f==px_i)&&(py_f==py_i-1))||((px_f==px_i)&&(py_f==py_i+1))||((px_f==px_i+1)&&(py_f==py_i))||((px_f==px_i-1)&&(py_f==py_i))){
             move=true;
        }else{
            if(direccion=='v'){
                if(((px_f==px_i+1)&&(py_f==py_i-1))||((px_f==px_i+1)&&(py_f==py_i+1))){
                      move=true;
                }
            }else{
                if(((px_f==px_i-1)&&(py_f==py_i-1))||((px_f==px_i-1)&&(py_f==py_i+1))){
                    move=true;
                }
            }
        }
    }

    return move;
}
char Lance::get_name(){
    return name;
}

char Lance::get_direcction(){
    return direccion;
}

void Lance::set_direcction(char dir){
    direccion=dir;
}

bool Lance::is_promotion(){
    return promotion;

}
void Lance::promote(){
    promotion=true;
}
bool Lance::is_save_promotion(){
    return save_promotion;
}
void Lance::set_save_promotion(bool s){
    save_promotion=s;
}

void Lance::set_original_form(){
    promotion=false;
}

bool Lance::check_promotion(int px){
    bool move=false;
    if(direccion=='v'){
        if(px==limit_promotion_up)
            move=true;
    }else{
        if(px==limit_promotion_down)
            move=true;
    }
    return move;
}

void Lance::set_free_boxes(bool p)
{
    free_boxes=p;
}

bool Lance::is_check_the_king(int px_i, int px_f, int py_i, int py_f)
{
    return valid_motion(px_i, px_f, py_i,  py_f);
}
