#ifndef BOARD_H
#define BOARD_H

class Piece;
class Board{
private:
    Piece ***board;
public: 
    Board();
    void set_piece(Piece *p, int px, int py);
    Piece *get_piece(int px, int py);
    bool is_empty(int px, int py);
    void update(int px_i, int px_f, int py_i,int py_f);
    void graph();
    ~Board();
};

#endif // BOARD_H
