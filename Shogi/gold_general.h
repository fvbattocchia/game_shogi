#ifndef GOLD_GENERAL_H
#define GOLD_GENERAL_H
#include<piece.h>

class Gold_general: public Piece{
public:
    Gold_general(char n, char d);
    virtual bool valid_motion(int px_i, int px_f, int py_i,int py_f);
    virtual char get_name();
    virtual char get_direcction();
    virtual void set_direcction(char dir);

    virtual bool is_promotion();
    virtual void promote();
    virtual bool is_save_promotion();
    virtual void set_save_promotion(bool s);
    virtual void set_original_form();
    virtual bool check_promotion(int px);

    virtual void set_free_boxes(bool p);
    virtual bool is_check_the_king(int px_i, int px_f, int py_i,int py_f);
};


#endif // GOLD_GENERAL_H
