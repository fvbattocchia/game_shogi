#include "bishop.h"

Bishop::Bishop(char n, char d):Piece(n,d){
    free_boxes= false;
}

bool Bishop::valid_motion(int px_i, int px_f, int py_i, int py_f){
    bool move=false;
    if(free_boxes){
        move=true;
        free_boxes=false;
    }else{
        if(promotion){
            if(((px_f==px_i+1)&&(py_f==py_i))||((px_f==px_i-1)&&(py_f==py_i))||((px_f==px_i)&&(py_f==py_i+1))||((px_f==px_i)&&(py_f==py_i-1))){
                 move=true;
            }
        }
    }
    return move;
}
char Bishop::get_name(){
    return name;
}

char Bishop::get_direcction(){
    return direccion;
}

void Bishop::set_direcction(char dir){
    direccion=dir;
}

bool Bishop::is_promotion(){
    return promotion;

}
void Bishop::promote(){
    promotion=true;
}
bool Bishop::is_save_promotion(){
    return save_promotion;
}
void Bishop::set_save_promotion(bool s){
    save_promotion=s;
}

void Bishop::set_original_form(){
    promotion=false;
}

bool Bishop::check_promotion(int px){
    return false;
}

void Bishop::set_free_boxes(bool p)
{
    free_boxes=p;
}

bool Bishop::is_check_the_king(int px_i, int px_f, int py_i, int py_f){
    return valid_motion(px_i, px_f, py_i,  py_f);
}
