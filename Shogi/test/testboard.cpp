#include "testboard.h"
#include "board.h"
#include "piece.h"
#include <pawn.h>
#define num_pawn 9
TestBoard::TestBoard(QObject *parent) : QObject(parent){


}

void TestBoard::test_board(){

    int px;
    int py;

    Board * shoji_board;
    shoji_board = new Board();
    for(int i=0;i<num_pawn;++i)
        shoji_board->set_piece(new Pawn('P','v'),2,i);
    for(int i=0;i<num_pawn;++i)
        shoji_board->set_piece(new Pawn('P','^'),6,i);
    px=2;
    py=1;
    QCOMPARE(shoji_board->get_piece(px,py)->get_direcction(),'v');
    QCOMPARE(shoji_board->get_piece(px,py)->get_name(),'P');
    QCOMPARE(shoji_board->is_empty(px,py),false);
    QCOMPARE(shoji_board->get_piece(px,py)->valid_motion(px,px+1,py,py),true);
    shoji_board->update(px,px+1,py,py);
    QCOMPARE(shoji_board->is_empty(px,py),true);
    QCOMPARE(shoji_board->get_piece(px+1,py)->valid_motion(px,px+5,py+1,py+5),false);

    px=2;
    py=4;
    QCOMPARE(shoji_board->get_piece(px,py)->is_promotion(),false);
    QCOMPARE(shoji_board->get_piece(px,py)->is_save_promotion(),false);
    shoji_board->get_piece(px,py)->promote();
    QCOMPARE(shoji_board->get_piece(px,py)->is_promotion(),true);
    shoji_board->get_piece(px,py)->set_original_form();
    QCOMPARE(shoji_board->get_piece(px,py)->is_promotion(),false);
    QCOMPARE(shoji_board->get_piece(px,py)->check_promotion(8),true);
    shoji_board->graph();
    delete shoji_board;
}
