#ifndef TESTBOARD_H
#define TESTBOARD_H

#include <QObject>
#include <QtTest/QtTest>
class TestBoard : public QObject
{
    Q_OBJECT
public:
    explicit TestBoard(QObject *parent = 0);

private slots:
    void test_board();
};

#endif // TESTBOARD_H
