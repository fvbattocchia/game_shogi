#ifndef TESTPLAYER_H
#define TESTPLAYER_H

#include <QObject>
#include <QtTest/QtTest>

class TestPlayer : public QObject
{
    Q_OBJECT
public:
    explicit TestPlayer(QObject *parent = 0);

private slots:
    void test_player();
};

#endif // TESTPLAYER_H
