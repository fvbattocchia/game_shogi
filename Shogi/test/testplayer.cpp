#include "testplayer.h"
#include "player.h"
#include "piece.h"
#include "pawn.h"
TestPlayer::TestPlayer(QObject *parent) : QObject(parent){


}

void TestPlayer::test_player(){
    Player *player_one = new Player('^');
    Piece *p1 =new Pawn('P','v');
    Piece *p2 =new Pawn('P','v');
    QCOMPARE(p1->get_direcction(),'v');
    QCOMPARE(player_one->get_direction(),'^');
    player_one->capture_piece(p1);
    QCOMPARE(p1->get_direcction(),'^');
    player_one->capture_piece(p2);

    QCOMPARE(player_one->get_piece('P')->get_name(),'P');

    player_one->print_captured_pieces();
    QCOMPARE(player_one->get_captured_number(),2);

    player_one->free_piece('P');
    QCOMPARE(player_one->get_captured_number(),1);

    player_one->free_piece('P');
    QCOMPARE(player_one->get_captured_number(),0);

    player_one->print_captured_pieces();

    delete player_one;

}
