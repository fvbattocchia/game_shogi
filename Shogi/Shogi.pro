QT += core
QT -= gui
#QT += testlib
CONFIG += c++11

TARGET = Shogi
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    #main_test.cpp \
    player.cpp \
    piece.cpp \
    pawn.cpp \
    game.cpp \
    board.cpp \
    #test/testboard.cpp \
    #test/testplayer.cpp \
    gold_general.cpp \
    king.cpp \
    rook.cpp \
    bishop.cpp \
    silver_general.cpp \
    knight.cpp \
    lance.cpp



HEADERS += \
    player.h \
    piece.h \
    pawn.h \
    game.h \
    board.h \
    #test/testboard.h \
    #test/testplayer.h \
    gold_general.h \
    king.h \
    rook.h \
    bishop.h \
    silver_general.h \
    knight.h \
    lance.h
