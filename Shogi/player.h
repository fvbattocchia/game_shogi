#ifndef PLAYER_H
#define PLAYER_H
#include <list>
using namespace std;
class Piece;
class Player{
private:
    char direction;
    list <Piece *> p_catch;
public:
    Player(char dir);
    void capture_piece(Piece *p);
    Piece * get_piece(char);
    void free_piece(char);
    int get_captured_number();
    void print_captured_pieces();
    void clear_list();
    char get_direction();

    ~Player();
};

#endif // PLAYER_H
