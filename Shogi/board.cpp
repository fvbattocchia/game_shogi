#include "board.h"
#include "piece.h"
#include<iostream>
#include<string>
using namespace std;
#define row  9
#define column  9

Board::Board(){
    board = new Piece**[row];
    for (int i = 0; i < row; ++i)
        board[i] = new Piece*[column];

    for(int i=0 ; i<row ; i++){
        for(int j=0 ; j<column ; j++){
            board[i][j] = NULL;
        }
    }
}

void Board::set_piece(Piece *p, int px, int py){
     board[px][py]=p;
}

Piece * Board::get_piece(int px, int py){
    return board[px][py];
}

bool Board::is_empty(int px, int py){
    bool empty=false;
    if(board[px][py] == NULL)
        empty=true;
    return empty;
}

void Board::update(int px_i, int px_f, int py_i, int py_f){
    set_piece(get_piece(px_i,py_i),px_f,py_f);
    set_piece(nullptr,px_i,py_i);
}

void Board::graph(){
    cout<<"   ";
    for (int i = 0; i < column; ++i)	{
        cout<<" "<<i<<"  ";
    }
    cout <<endl<<"   ------------------------------------"<< endl;
    for (int i = 0; i < row ; ++i){
        cout<< i << " |";
        for (int j = 0; j < column; ++j){
            if(board[i][j] != NULL){
                //pregunto si esta promocionada y segun eso ponfo un nombre u otro
                if(board[i][j]->is_promotion()){
                    cout <<"+"<<board[i][j]->get_name()<<board[i][j]->get_direcction() <<"|";
                }else{
                    cout<<" "<<board[i][j]->get_name()<<board[i][j]->get_direcction() <<"|";
                }
            }else{
                cout << "   |";
            }
        } 
        cout<<endl<< "   ------------------------------------"<< endl;
    }

}

Board::~Board(){
    for(int i=0;i<row;i++){
        for(int j=0;j<column;j++){
            delete[] board[i][j];
        }
    }
    for(int i=0;i<row;i++){
        delete[] board[i];
    }
    delete[] board;
}
