#include "silver_general.h"


Silver_general::Silver_general(char n, char d):Piece(n,d){

}

bool Silver_general::valid_motion(int px_i, int px_f, int py_i, int py_f){
    bool move=false;
    if(!promotion){
        if(((px_f==px_i+1)&&(py_f==py_i-1))||((px_f==px_i+1)&&(py_f==py_i+1))||((px_f==px_i-1)&&(py_f==py_i+1))||((px_f==px_i-1)&&(py_f==py_i-1))){
             move=true;
        }else{
            if(direccion=='v'){
                if((px_f==px_i+1) && (py_f==py_i))
                    move=true;
            }else{
                if((px_f==px_i-1) && (py_f==py_i))
                    move=true;
            }
        }
    }else{
        if(((px_f==px_i)&&(py_f==py_i-1))||((px_f==px_i)&&(py_f==py_i+1))||((px_f==px_i+1)&&(py_f==py_i))||((px_f==px_i-1)&&(py_f==py_i))){
             move=true;
        }else{
            if(direccion=='v'){
                if(((px_f==px_i+1)&&(py_f==py_i-1))||((px_f==px_i+1)&&(py_f==py_i+1))){
                      move=true;
                }
            }else{
                if(((px_f==px_i-1)&&(py_f==py_i-1))||((px_f==px_i-1)&&(py_f==py_i+1))){
                    move=true;
                }
            }
        }
    }
    return move;
}
char Silver_general::get_name(){
    return name;
}

char Silver_general::get_direcction(){
    return direccion;
}

void Silver_general::set_direcction(char dir){
    direccion=dir;
}

bool Silver_general::is_promotion(){
    return promotion;

}
void Silver_general::promote(){
    promotion=true;
}
bool Silver_general::is_save_promotion(){
    return save_promotion;
}
void Silver_general::set_save_promotion(bool s){
    save_promotion=s;
}

void Silver_general::set_original_form(){
    promotion=false;
}

bool Silver_general::check_promotion(int px){
    return false;
}

void Silver_general::set_free_boxes(bool p){


}

bool Silver_general::is_check_the_king(int px_i, int px_f, int py_i, int py_f){
    return valid_motion(px_i, px_f, py_i,  py_f);
}
