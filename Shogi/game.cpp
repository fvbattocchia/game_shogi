#include<iostream>
using namespace std;
#include "game.h"
#include <player.h>
#include <pawn.h>
#include "gold_general.h"
#include "silver_general.h"
#include <board.h>
#include "rook.h"
#include "bishop.h"
#include "piece.h"
#include "king.h"
#include "knight.h"
#include "lance.h"
#define num_pawn 9
#define Promotion_up 6
#define Promotion_down 2
#define limit_board num_pawn

Game::Game(){
    stop_game=false;
    init_game();
}

void Game::init_game(){
    shoji_board = new Board();
    shoji_board->set_piece(new Rook('R','v'),1,1);
    shoji_board->set_piece(new Bishop('B','v'),1,7);
    shoji_board->set_piece(new Lance('L','v'),0,0);
    shoji_board->set_piece(new Lance('L','v'),0,8);
    shoji_board->set_piece(new Knight('N','v'),0,1);
    shoji_board->set_piece(new Knight('N','v'),0,7);
    shoji_board->set_piece(new Silver_general('S','v'),0,2);
    shoji_board->set_piece(new Silver_general('S','v'),0,6);
    shoji_board->set_piece(new Gold_general('G','v'),0,3);
    shoji_board->set_piece(new Gold_general('G','v'),0,5);
    shoji_board->set_piece(new King('K','v'),0,4);
    for(int i=0;i<num_pawn;++i)
        shoji_board->set_piece(new Pawn('P','v'),2,i);

    shoji_board->set_piece(new Rook('R','^'),7,1);
    shoji_board->set_piece(new Bishop('B','^'),7,7);
    shoji_board->set_piece(new Lance('L','^'),8,0);
    shoji_board->set_piece(new Lance('L','^'),8,8);
    shoji_board->set_piece(new Knight('N','^'),8,1);
    shoji_board->set_piece(new Knight('N','^'),8,7);
    shoji_board->set_piece(new Silver_general('S','^'),8,2);
    shoji_board->set_piece(new Silver_general('S','^'),8,6);
    shoji_board->set_piece(new Gold_general('G','^'),8,3);
    shoji_board->set_piece(new Gold_general('G','^'),8,5);
    shoji_board->set_piece(new King('K','^'),8,4);
    for(int i=0;i<num_pawn;++i)
        shoji_board->set_piece(new Pawn('P','^'),6,i);

    player_one = new Player('^');
    player_two = new Player('v');
}

bool Game::check_point(int px, int py,char player_dir){
    bool valid_point=false;
    if(!shoji_board->is_empty(px,py)){
        if(shoji_board->get_piece(px,py)->get_direcction()==player_dir){
            valid_point=true;
        }
    }
    return valid_point;
}

void Game::turn(Player *player){
    int pieces ;
    int op;
    shoji_board->graph();
    cout<<endl;
    pieces=player->get_captured_number();
    cout<<endl;
    if(pieces>0){
        cout<<"Para mover una pieza ingrese: 1"<<endl;
        cout<<"Para reingresar una pieza ingrese: 2"<<endl;
        cin>>op;
        switch (op) {
        case 1:
            move_piece(player);
            break;
        case 2:
            reentry_piece(player);
            break;
        default:
            cout<<"Ninguna opcion es correcta"<<endl;
            break;
        }
    }else{
        move_piece(player);
    }
}

void Game::find_king(char dir, int *px, int *py){
    for(int i=0;i<limit_board;++i){
        for(int j=0;j<limit_board;++j){
            if((shoji_board->get_piece(i,j)->get_name()=='K')&& (shoji_board->get_piece(i,j)->get_direcction()!=dir))
                *px=i;
                *py=j;
                return;
        }
    }
}

void Game::play(){
    cout<<"Comienza la partida de la ajedrez japonesa"<<endl;
    cout<<"Comienza el jugador con la direccion "<< player_one->get_direction()<<endl;
    cout<<"Luego sigue el jugador con la direccion "<< player_two->get_direction()<<endl;
    cout<<endl;
    while (true) {
      cout<<"Turno del jugador 1"<<endl;
      cout<<endl;
      turn(player_one);
      if(stop_game){
         cout<<"El ganador es el jugador 1"<<endl;
         break;
      }
      cout<<"Turno del jugador 2"<<endl;
      cout<<endl;
      turn(player_two);
      if(stop_game){
         cout<<"El ganador es el jugador 2"<<endl;
         break;
      }
    }
}

void Game::move_piece(Player* player){
    int px_i,px_f,py_i,py_f,px_k,py_k;
    bool free_boxes;
    cout<<"Ingrese el punto x y luego el punto y donde se encuentra la pieza que quiere mover: "<<endl;
    cin>>px_i;
    cin>>py_i;
    cout<<"Ingrese el punto x y luego el punto y donde quiere mover la pieza: "<<endl;
    cin>>px_f;
    cin>>py_f;
    find_king(player->get_direction(),&px_k,&py_k);
    if(px_i>=0&&py_i>=0&&px_i<9&&py_i<9&&px_f>=0&&py_f>=0&&px_f<9&&py_f<9){
        if(check_point(px_i,py_i,player->get_direction())){//verifica punto de origen
            if((shoji_board->get_piece(px_i,py_i)->get_name()=='R')||(shoji_board->get_piece(px_i,py_i)->get_name()=='L')||(shoji_board->get_piece(px_i,py_i)->get_name()=='B')){
                free_boxes=check_free_boxes(shoji_board->get_piece(px_i,py_i)->get_name(),px_i,px_f,py_i,py_f,player->get_direction());
                shoji_board->get_piece(px_i,py_i)->set_free_boxes(free_boxes);
            }
            if(shoji_board->get_piece(px_i,py_i)->valid_motion(px_i,px_f,py_i,py_f)){//verifica movimiento
                //verifica punto de destino
                if(shoji_board->is_empty(px_f,py_f)){
                    shoji_board->update(px_i,px_f,py_i,py_f);
                    promote(shoji_board->get_piece(px_f,py_f),px_f);
                    if(shoji_board->get_piece(px_f,py_f)->is_check_the_king(px_f,px_k,py_f,px_k)){
                        cout<<"Jaque"<<endl;
                    }
                }else{
                    if(!check_point(px_f,py_f,player->get_direction())){//la pieza es del jugador contrario
                        if(shoji_board->get_piece(px_f,py_f)->get_name()!='K'){
                            player->capture_piece(shoji_board->get_piece(px_f,py_f));
                            cout<<"Pieza capturada"<<endl;
                            shoji_board->set_piece(nullptr,px_f,py_f);
                            shoji_board->update(px_i,px_f,py_i,py_f);
                            promote(shoji_board->get_piece(px_f,py_f),px_f);
                            if(shoji_board->get_piece(px_f,py_f)->is_check_the_king(px_f,px_k,py_f,px_k)){
                                cout<<"Jaque"<<endl;
                            }
                        }else{
                            stop_game=true;
                        }
                     }else{
                        cout<<"punto final invalido"<<endl;
                     }
                }
            }else{
                cout<<"movimiento invalido"<<endl;
            }
        }else{
            cout<<"punto de origen invalido"<<endl;
        }
    }else{
        cout<<"punto fuera del tablero"<<endl;
    }
}

void Game::reentry_piece(Player *player){
    int px=0,py=0;
    char index='y';
    player->print_captured_pieces();
    cout<<"Ingrese la letra inicial de la pieza que quiere reingresar:"<<endl;
    cin>>index;
    if(index=='P'||index=='R'||index=='B'||index=='G'||index=='S'||index=='N'||index=='L'){
        cout<<"Ingrese el punto x y luego el punto y donde desea ingresar la pieza"<<endl;
        cin>>px;
        cin>>py;
        if(px>=0&&py>=0&&px<9&&py<9){
            if(shoji_board->is_empty(px,py)){
                shoji_board->set_piece(player->get_piece(index),px,py);
                player->free_piece(index);
            }else{
                cout<<"El punto no esta libre";
            }
        }else{
            cout<<"El punto esta fuera del tablero";
        }
    }else{
        cout<<"letra invalida"<<endl;
    }
}
void Game::promote(Piece *p,int px){
    char op='n';
    if(!p->is_promotion() && p->get_name()!='K' && p->get_name()!='G'){
        if(p->is_save_promotion()){
            if(p->check_promotion(px)){//es obligatorio promover
                p->promote();
            }else{
                cout<<"si quiere promnocionar la pieza ingrese: y"<<endl;
                cout<<"si quiere promnocionarla en otro turno ingrese: n"<<endl;
                cin>>op;
                if(op=='y'){
                    p->promote();
                }
            }
        }else {
            if((p->get_direcction()=='v' && px>=Promotion_up ) ||( p->get_direcction()=='^'&& px<=Promotion_down)){ //cumple los limites
                if(p->check_promotion(px)){
                    p->promote();
                }else{
                    cout<<"si quiere promnocionar la pieza ingrese: y"<<endl;
                    cout<<"si quiere promnocionarla en otro turno ingrese: n"<<endl;
                    cin>>op;
                    if(op=='y'){
                        p->promote();
                    }else{
                        p->set_save_promotion(true);
                    }
                }
            }
        }
    }
}
bool Game::check_free_boxes(char nombre,int px_i,int py_i,int px,int py,char dir){
    bool free_boxes =true;
    int i;
    switch (nombre) {
    case 'P':
        if(py_i==py){
            if(px_i<px){
                px_i++;
                for(i=px_i;i<=px;++i){
                    if(!shoji_board->is_empty(i,py_i)){
                        free_boxes=false;
                    }
                }
            }else{
                px_i--;
                for(i=px_i;i>=px;--i){
                    if(!shoji_board->is_empty(i,py_i)){
                        free_boxes=false;
                    }
                }
            }
        }else{
            if(py_i<py){
                py_i++;
                for(i=py_i;i<=py;++i){
                    if(!shoji_board->is_empty(px_i,i)){
                        free_boxes=false;
                    }
                }
            }else{
                py_i--;
                for(i=py_i;i>=py;--i){
                    if(!shoji_board->is_empty(px_i,i)){
                        free_boxes=false;
                    }
                }
            }
        }
        break;
    case 'B':
        if(px_i<px&&py_i<py){
            px_i++;
            py_i++;
            for(i=px_i;i<=px;++i){
                if(!shoji_board->is_empty(i,py_i++)){
                    free_boxes=false;
                }
            }
        }else if(px_i<px&&py_i>py){
            px_i++;
            py_i--;
            for(i=px_i;i<=px;++i){
                if(!shoji_board->is_empty(i,py--)){
                    free_boxes=false;
                }
            }
          }else if(px_i>px&&py_i>py){
            px_i--;
            py_i--;
            for(i=px_i;i>=px;--i){
                if(!shoji_board->is_empty(i,py--)){
                    free_boxes=false;
                }
            }
        }else if(px_i>px&&py_i<py){
            px_i--;
            py_i++;
            for(i=px_i;i>=px;--i){
                if(!shoji_board->is_empty(i,py++)){
                    free_boxes=false;
                }
            }
        }
        break;
    case 'L':
        if(dir=='v'){
            px_i++;
            for(i=px_i;i<=px;++i){
                if(!shoji_board->is_empty(i,py_i)){
                    free_boxes=false;
                }
            }
        }else{
            px_i--;
            for(i=px_i;i>=px;--i){
                if(!shoji_board->is_empty(i,py_i)){
                    free_boxes=false;
                }
            }
        }

        break;
    default:
        break;
    }
    return free_boxes;
}
Game::~Game(){
    delete player_one;
    delete player_two;
    delete shoji_board;
}
