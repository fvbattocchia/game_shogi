#include "pawn.h"
#define limit_promotion_up 8
#define limit_promotion_down 0
Pawn::Pawn(char n, char d):Piece(n,d){

}

bool Pawn::valid_motion(int px_i, int px_f, int py_i, int py_f){
    bool move=false;
    if(!promotion){
        if(direccion=='v'){
            if((px_f==px_i+1) && (py_f==py_i))
                move=true;
        }else{
            if((px_f==px_i-1) && (py_f==py_i))
                move=true;
        }
    }else{
        if(((px_f==px_i)&&(py_f==py_i-1))||((px_f==px_i)&&(py_f==py_i+1))||((px_f==px_i+1)&&(py_f==py_i))||((px_f==px_i-1)&&(py_f==py_i))){
             move=true;
        }else{
            if(direccion=='v'){
                if(((px_f==px_i+1)&&(py_f==py_i-1))||((px_f==px_i+1)&&(py_f==py_i+1))){
                      move=true;
                }
            }else{
                if(((px_f==px_i-1)&&(py_f==py_i-1))||((px_f==px_i-1)&&(py_f==py_i+1))){
                    move=true;
                }
            }
        }
    }

    return move;
}
char Pawn::get_name(){
    return name;
}

char Pawn::get_direcction(){
    return direccion;
}

void Pawn::set_direcction(char dir){
    direccion=dir;
}

bool Pawn::is_promotion(){
    return promotion;

}
void Pawn::promote(){
    promotion=true;
}
bool Pawn::is_save_promotion(){
    return save_promotion;
}
void Pawn::set_save_promotion(bool s){
    save_promotion=s;
}

void Pawn::set_original_form(){
    promotion=false;
}

bool Pawn::check_promotion(int px){
    bool move=false;
    if(direccion=='v'){
        if(px==limit_promotion_up)
            move=true;
    }else{
        if(px==limit_promotion_down)
            move=true;
    }
    return move;
}

void Pawn::set_free_boxes(bool p)
{

}

bool Pawn::is_check_the_king(int px_i, int px_f, int py_i, int py_f)
{
    return valid_motion(px_i, px_f, py_i,  py_f);
}




