#ifndef PIECE_H
#define PIECE_H

#include<string>
using namespace std;

class Piece{

public:
    Piece();
    Piece(char name, char direccion);
    virtual bool valid_motion(int px_i, int px_f, int py_i,int py_f)=0;
    virtual char get_name()=0;
    virtual char get_direcction()=0;
    virtual void set_direcction(char dir)=0;
    virtual bool is_promotion()=0;
    virtual void promote()=0;
    virtual bool is_save_promotion()=0;
    virtual void set_save_promotion(bool s)=0;
    virtual void set_original_form()=0;
    virtual bool check_promotion(int px)=0;
    virtual void set_free_boxes(bool p)=0;
    virtual bool is_check_the_king(int px_i, int px_f, int py_i,int py_f)=0;

    char name;
    char direccion;
    bool promotion;
    bool save_promotion;
};

#endif // PIECE_H
