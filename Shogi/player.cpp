#include "player.h"
#include "piece.h"
#include <iterator>
#include<iostream>

Player::Player(char dir){
    direction= dir;

}

void Player::capture_piece(Piece *p){
    p->set_direcction(direction);
    if(p->is_promotion()){
        p->set_original_form();
    }
    if(p->is_save_promotion()){
        p->set_save_promotion(false);
    }
    p_catch.push_back(p);
}

Piece *Player::get_piece(char e){
    list<Piece *>::iterator iter;
    for(iter = p_catch.begin(); iter != p_catch.end(); iter++){
       if((*iter)->get_name()==e){
           break;
       }
    }
    return *iter;
}

void Player::free_piece(char e){
    list<Piece *>::iterator iter;
    for(iter = p_catch.begin(); iter != p_catch.end(); iter++){
       if((*iter)->get_name()==e){
           p_catch.erase (iter);
           break;
       }
    }
}

int Player::get_captured_number(){
    return p_catch.size();
}

void Player::print_captured_pieces(){
    list<Piece *>::iterator iter;
    for(iter = p_catch.begin(); iter != p_catch.end(); iter++){
       cout<<"=> "<<(*iter)->get_name()<<endl;
    }
}

void Player::clear_list(){
    p_catch.clear();
}

char Player::get_direction(){
    return direction;
}

Player::~Player(){
    clear_list();
}
