#include "rook.h"

Rook::Rook(char n, char d):Piece(n,d){
    free_boxes= false;
}

bool Rook::valid_motion(int px_i, int px_f, int py_i, int py_f){
    bool move=false;
    if(free_boxes){
        move=true;
        free_boxes=false;
    }else{
        if(promotion){
            if(((px_f==px_i+1)&&(py_f==py_i-1))||((px_f==px_i+1)&&(py_f==py_i+1))||((px_f==px_i-1)&&(py_f==py_i+1))||((px_f==px_i-1)&&(py_f==py_i-1))){
                 move=true;
            }
        }
    }
    return move;
}
char Rook::get_name(){
    return name;
}

char Rook::get_direcction(){
    return direccion;
}

void Rook::set_direcction(char dir){
    direccion=dir;
}

bool Rook::is_promotion(){
    return promotion;

}
void Rook::promote(){
    promotion=true;
}
bool Rook::is_save_promotion(){
    return save_promotion;
}
void Rook::set_save_promotion(bool s){
    save_promotion=s;
}

void Rook::set_original_form(){
    promotion=false;
}

bool Rook::check_promotion(int px){
    return false;
}

void Rook::set_free_boxes(bool p){
    free_boxes= p;

}

bool Rook::is_check_the_king(int px_i, int px_f, int py_i, int py_f)
{
    return valid_motion(px_i, px_f, py_i,  py_f);
}

