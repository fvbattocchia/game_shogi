# Shogi
Juego Ajedrez Japones
funcionalidades implementadas:
- movimiento de piezas
- movimiento de piezas promocionadas
- re introducir piezas capturadas
- jaque y jaque mate

Para ejecutar los test unitarios se debe descomentar en el archivo .pro:
    #main_test.cpp \
    #test/testboard.cpp \
    #test/testplayer.cpp \
    #test/testboard.h \
    #test/testplayer.h \
descomentar tambien la libreria #QT += testlib
y comentar: 
    main.cpp \

